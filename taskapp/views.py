from django.shortcuts import render
from django.shortcuts import render
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import serializers
from rest_framework.decorators import api_view,permission_classes
from .serializers import RegisterSerializer,LoginSerializer,AllProductSerializer,WishlistSerializer,CartProductSerializer
from .models import ProductModel,CartModel
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
# Create your views here.
from .service import generate_token
import json

@api_view(['POST'])
@permission_classes([AllowAny])
def Register(request):
    serializer = RegisterSerializer(data=request.data)
    if serializer.is_valid():
        user=serializer.save()
        if user:
            return Response(serializer.data,status=status.HTTP_201_CREATED)
    print(serializer.errors)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([AllowAny])
def sign_in(request):
    serializer = LoginSerializer(data=request.data)
    print(serializer)
    if serializer.is_valid():
        user = User.objects.get(email=serializer.data["email"])
        user_auth = authenticate(username=user.username, password=serializer.data["password"])
        if user_auth:
            token = generate_token(user)
        return Response(token, status=status.HTTP_200_OK)
    print(serializer.errors)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([AllowAny])
def get_all_prodcuts(request):
    product = ProductModel.objects.all()
    serializer = AllProductSerializer(product,many=True, context={"request": request})
    if serializer:
       
        content=serializer.data
       
        return Response(content, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([AllowAny])
def get_single(request):
    
    singleprodcutSerializer=SingleProductSerializer(data=request.data)
    if singleprodcutSerializer:
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def wishlist(request):
   
    serializer = WishlistSerializer(data=request.data,context={"request":request})
    if serializer.is_valid():
        wishlist_data = serializer.save()
        if wishlist_data:
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])  
def get_all_cartProducts(request):
    product = CartModel.objects.filter(user=request.user.id)
    serializer = CartProductSerializer(product,many=True, context={"request": request})
    print(serializer.data)
    if serializer:
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)